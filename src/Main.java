import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * La classe principale pour exécuter le jeu de héro (Hero Game)
 */
public class Main {

    /**
     * Le point d'entrée du programme.
     *
     * @param args Les arguments de la ligne de commande.
     */
    public static void main(String[] args) 
    {
        //Déclaration des Variables
        char[][] map;
        File stepsFile;
        Scanner stepsScanner;
        String positionLine;
        String[] positionCoords;

        int heroX;
        int heroY;

        String movements;

        try {
            // Charger la carte à partir du fichier texte
            map = loadMapFromFile("./src/carte.txt");

            // Charger les étapes depuis le fichier "déplacements.txt"
            stepsFile = new File("./src/déplacements.txt");
            stepsScanner = new Scanner(stepsFile);

            // Lire la position du héros depuis la première ligne de déplacements.txt
            positionLine = stepsScanner.nextLine();
            positionCoords = positionLine.split(",");
            heroX = Integer.parseInt(positionCoords[0].trim());
            heroY = Integer.parseInt(positionCoords[1].trim());

            // Valider la position du héros
            if (!isValidPosition(map, heroX, heroY))
            {
                System.out.println("Position initiale du héros invalide.");
                stepsScanner.close();
                return;
            }

            // Lire les mouvements depuis la deuxième ligne de déplacements.txt
            movements = stepsScanner.nextLine().trim().toUpperCase();

            stepsScanner.close();

            // Valider les mouvements
            if (!isValidMovements(map, movements, heroX, heroY))
            {
                System.out.println("\u001B[31mMouvements invalides !\u001B[0m");
                return;
            }

            HeroGame game = new HeroGame(map, heroX, heroY);

            game.updateHeroPosition(movements);


            System.out.println(game.getCurrentHeroPosition());
            System.out.println();
            System.out.println("\u001B[32mÉtat final de la carte :\u001B[0m");
            game.displayMap();

        }
        catch (FileNotFoundException e)
        {
            System.out.println("Le fichier de la carte ou le fichier des étapes n'a pas été trouvé." + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Méthode auxiliaire pour charger la carte à partir d'un fichier texte.
     *
     * @param filePath Le chemin du fichier de la carte.
     * @return La carte chargée sous forme d'un tableau 2D de caractères.
     * @throws FileNotFoundException Si le fichier de la carte n'est pas trouvé.
     */
    private static char[][] loadMapFromFile(String filePath) throws FileNotFoundException
    {
        File file = new File(filePath);
        Scanner scanner = new Scanner(file);

        int numRows = 0;
        int numColumns = 0;

        String line;

        // Compter le nombre de lignes et le nombre maximum de colonnes dans le fichier
        while (scanner.hasNextLine())
        {
            numRows++;
            line = scanner.nextLine();
            numColumns = Math.max(numColumns, line.length());
        }

        scanner.close();

        char[][] map = new char[numRows][numColumns];
        scanner = new Scanner(file);

        int row = 0;
        while (scanner.hasNextLine())
        {
            line = scanner.nextLine();
            for (int col = 0; col < line.length(); col++)
            {
                map[row][col] = line.charAt(col);
            }
            row++;
        }
        return map;
    }

    /**
     * Vérifie si la position donnée est valide sur la carte.
     *
     * @param map La carte.
     * @param x   La coordonnée x.
     * @param y   La coordonnée y.
     * @return Vrai si la position est valide, faux sinon.
     */
    private static boolean isValidPosition(char[][] map, int x, int y)
    {
        if (x < 0 || y < 0 || y >= map.length || x >= map[y].length || map[y][x] == '#')
        {
            return false;
        }
        return true;
    }

    /**
     * Vérifie si les mouvements donnés sont valides.
     *
     * @param map        La carte.
     * @param movements  La chaîne de mouvements.
     * @param startX     La coordonnée X de départ du héros.
     * @param startY     La coordonnée Y de départ du héros.
     * @return Vrai si les mouvements sont valides, faux sinon.
     */
    private static boolean isValidMovements(char[][] map, String movements, int startX, int startY)
    {
        int currentX = startX;
        int currentY = startY;

        for (char move : movements.toCharArray())
        {
            if (move != 'N' && move != 'O' && move != 'S' && move != 'E')
            {
                return false; // Caractère de mouvement invalide
            }
            switch (move)
            {
                case 'N':
                    currentY--;
                    break;
                case 'O':
                    currentX--;
                    break;
                case 'S':
                    currentY++;
                    break;
                case 'E':
                    currentX++;
                    break;
            }
            if (currentX < 0 || currentY < 0 || currentY >= map.length || currentX >= map[currentY].length || map[currentY][currentX] == '#')
            {
                return false; // Le mouvement mène à une position invalide (#)
            }
        }
        // Vérifier s'il reste des caractères dans la chaîne de mouvements
        if (movements.length() > 1 && currentX == startX && currentY == startY)
        {
            return false; // Combinaison de caractères invalide
        }
        return true; // Tous les mouvements sont valides
    }

}
