/**
 * Classe représentant le jeu de héro dans un monde dangereux.
 */
public class HeroGame {

    //Attributs de la classe : ces attributs utilisés seulement par cette classe
    private char[][] map;
    private int heroX;
    private int heroY;

    public HeroGame(){}
    /**
     * Constructeur de la classe HeroGame.
     * @param map La carte du monde.
     * @param heroX La position initiale X du héros.
     * @param heroY La position initiale Y du héros.
     */
    public HeroGame(char[][] map, int heroX, int heroY)
    {
        this.map = map;
        this.heroX = heroX;
        this.heroY = heroY;
    }

    /**
     * Méthode pour afficher l'état actuel du monde avec la position du héros.
     */
    char cell ;
    public void displayMap()
    {
        for (int i = 0; i < map.length; i++)
        {
            for (int j = 0; j < map[i].length; j++)
            {
                cell = map[i][j];
                if (cell == '#')
                {
                    System.out.print(cell); // Afficher l'élément de bois impénétrables
                }
                else if (i == heroY && j == heroX)
                {
                    System.out.print("\u001B[32mH\u001B[0m"); // Afficher le héros H avec la couleur Vert
                }
                else
                {
                    System.out.print(' '); // Afficher un espace vide
                }
            }
            System.out.println();
        }
    }

    /**
     * Méthode pour mettre à jour la position du héros en fonction des mouvements fournis.
     * @param movements Les mouvements du héros.
     */
    public void updateHeroPosition(String movements)
    {
        for (char move : movements.toCharArray())
        {
            switch (move)
            {
                case 'N':
                    if (heroY > 0 && map[heroY - 1][heroX] != '#')
                    {
                        heroY--;
                    }
                    break;
                case 'O':
                    if (heroX > 0 && map[heroY][heroX - 1] != '#')
                    {
                        heroX--;
                    }
                    break;
                case 'S':
                    if (heroY < map.length - 1 && map[heroY + 1][heroX] != '#')
                    {
                        heroY++;
                    }
                    break;
                case 'E':
                    if (heroX < map[heroY].length - 1 && map[heroY][heroX + 1] != '#')
                    {
                        heroX++;
                    }
                    break;
                default:
                    System.out.println("Mouvement invalide : " + move);
                    break;
            }
        }
    }

    /**
     * Méthode pour obtenir la position actuelle du héros.
     * @return La position actuelle du héros sous forme de chaîne.
     */
    public String getCurrentHeroPosition()
    {
        return "Position actuelle de \u001B[32mhéro H\u001B[0m : (" + heroX + ", " + heroY+")";
    }
}